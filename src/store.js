import { configureStore } from "@reduxjs/toolkit";
import gameSlice from "./reducers/gameSlice";

const store = (initialState) => {
  return configureStore({ reducer: { game: gameSlice } });
};
export default store;
