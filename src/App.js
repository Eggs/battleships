import GameLoop from "./components/GameLoop/GameLoop";

function App() {
  return (
    <div className="App">
      <GameLoop />
    </div>
  );
}

export default App;
