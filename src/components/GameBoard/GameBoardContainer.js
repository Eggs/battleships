import { Ship } from "../Ship/Ship";

import { useState } from "react";

const GameBoardContainer = () => {
  const [ships, setShips] = useState([]);

  const createGameBoard = () => {
    const gameBoard = [];
    for (let i = 0; i < 100; i++) {
      gameBoard.push({ isHit: false, id: 0 });
    }

    return gameBoard;
  };

  const [gameBoard, setGameBoard] = useState(createGameBoard());

  const checkIfShipsAreAround = (moves, shipLength) => {
    //const invalidPlaces = [9, 19, 29, 39, 49, 59, 69, 79, 89, 99];
    let isValid = true;

    // First check there are no ships in the squares we want to use.
    moves.forEach((move, i) => {
      if (gameBoard[move].id !== 0) {
        isValid = false;
      }
    });

    const sides = [9, 11, 1]; // Left: +9, -11, -1 || Right: -9, +11, +1
    const topAndBottom = 10; // Top: +10 || Bottom: -10

    moves.forEach((move, i) => {
      sides.forEach((side, i) => {
        if (
          typeof gameBoard[move + side] === "undefined" ||
          typeof gameBoard[move - side] === "undefined"
        ) {
          isValid = isValid;
        } else if (gameBoard[move + side].id !== 0) {
          isValid = false;
        } else if (gameBoard[move - side].id !== 0) {
          isValid = false;
        }
      });

      if (
        typeof gameBoard[move + topAndBottom] === "undefined" ||
        typeof gameBoard[move - topAndBottom] === "undefined"
      ) {
        isValid = isValid;
      } else if (gameBoard[move + topAndBottom].id !== 0) {
        isValid = false;
      } else if (gameBoard[move - topAndBottom].id !== 0) {
        isValid = false;
      }
    });

    return isValid;
  };

  const checkIfMoveIsValid = (payload, ship) => {
    const invalidPlaces = [9, 19, 29, 39, 49, 59, 69, 79, 89, 99];
    const endOfShip = parseInt(payload.id, 10) + ship.getLength() - 1;
    let moves = [];

    for (let i = parseInt(payload.id, 10); i <= endOfShip; i++) {
      moves.push(i);
    }

    const found = invalidPlaces.some((r) => moves.includes(r));

    if (found) {
      return false;
    } else {
      if (checkIfShipsAreAround(moves, ship.getLength())) {
        return true;
      } else {
        return false;
      }
    }
  };

  const addShipToBoard = (payload, newShip) => {
    const newBoard = gameBoard;

    if (checkIfMoveIsValid(payload, newShip)) {
      for (let i = 0; i < newShip.getLength(); i++) {
        newBoard.splice(parseInt(payload.id, 10) + i, 1, payload);
      }
      return setGameBoard(newBoard);
    } else {
      return gameBoard;
    }
  };

  const addShip = (e, length, gridPosition) => {
    const newShip = Ship(length, gridPosition);
    setShips([...ships, newShip]);
    addShipToBoard({ shipId: newShip.id, id: e.target.id }, newShip);
  };

  const receiveAttack = (idx) => {
    const ship = ships.find((e) => e.id === idx);
    if (ship) {
      const newBoard = gameBoard.map((cell) => {
        if (String(cell.id) === String(idx)) {
          return {
            ...cell,
            isHit: true,
          };
        } else {
          return cell;
        }
      });
      setGameBoard(newBoard);
      return ship.hit();
    } else {
      return "miss";
    }
  };

  const checkIfAllShipsAreSunk = () => {
    const filtered = ships.filter((ship) => !ship.isSunk);

    if (filtered.length > 0) {
      return false;
    } else {
      return true;
    }
  };

  return {
    ships,
    gameBoard,
    addShip,
    receiveAttack,
    checkIfAllShipsAreSunk,
    checkIfMoveIsValid,
  };
};

export default GameBoardContainer;
