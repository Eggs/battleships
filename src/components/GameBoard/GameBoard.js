//import { GameBoardLogic } from "./GameBoardLogic";
import { Ship } from "../Ship/Ship";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";

const useStyles = makeStyles(() => ({
  GameBoardContainer: {
    display: "grid",
    gridTemplateColumns: "repeat(10, 42px)",
    gap: "0px",
  },
  cellContainer: {
    width: "40px",
    height: "auto",
  },
  gameCell: {
    width: "40px",
    height: "40px",
    textAlign: "center",
    border: "2px solid grey",
    borderRadius: "2% 6% 5% 4% / 1% 1% 2% 4%",
  },
  gameCellHasShip: {
    width: "40px",
    height: "40px",
    textAlign: "center",
    border: "2px solid #333333",
    fontSize: "2rem",
    borderRadius: "2% 6% 5% 4% / 1% 1% 2% 4%",
  },
}));

const GameBoard = (props) => {
  const gameBoard = props.gameBoard;
  const { receiveAttack, addShip } = props;

  const classes = useStyles();

  let radioValue = 3;
  const onRadioClick = (e) => {
    radioValue = e.target.value;
  };

  return (
    <div id="#game-board-container" className={classes.GameBoardContainer}>
      <input
        type="radio"
        name="shipLength"
        value="3"
        onClick={(e) => onRadioClick(e)}
      />
      <input
        type="radio"
        name="shipLength"
        value="1"
        onClick={(e) => onRadioClick(e)}
      />
      {gameBoard.length !== 0 ? (
        gameBoard.map((cell, idx) => {
          return (
            <Card
              className={cell.id ? classes.gameCellHasShip : classes.gameCell}
              key={idx}
              id={idx}
              variant="outlined"
              onClick={(e) =>
                cell.id
                  ? receiveAttack(cell.id)
                  : addShip(e, parseInt(radioValue, 10), [3, 4, 5])
              }
            >
              {cell.id ? "S" : ""}
            </Card>
          );
        })
      ) : (
        <li
          key={0}
          id={0}
          onClick={() =>
            addShip({ ship: Ship(radioValue, [3, 4, 5]).getIsSunk(), idx: 0 })
          }
        >
          Blank
        </li>
      )}
    </div>
  );
};

export default GameBoard;
