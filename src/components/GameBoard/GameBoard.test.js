import GameBoardContainer from "./GameBoardContainer";

const game = GameBoardContainer();
const gameBoard = game.gameBoard;

test("Game board is generated correctly", () => {
  expect(gameBoard).toHaveLength(100);
});

test("Ships can be placed on game board", () => {
  game.addShip({ target: { id: 0 } }, 3, [3, 4, 5]);
  expect(game.getShips().length).toBeGreaterThan(0);
});

test("Ships placed on the board can receive an attack", () => {
  expect(game.receiveAttack(game.getShips()[0].id)).not.toContain("miss");
});
