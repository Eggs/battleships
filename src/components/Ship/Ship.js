import { v4 as uuidv4 } from "uuid";

const Ship = (length, gridPosition) => {
  const id = uuidv4();
  let isSunk = false;
  let isVertical = false;
  let lives = length;
  const getLives = () => lives;
  const hit = () => {
    if (lives > 0) {
      lives -= 1;
      return lives; // return the number of lives left when hit.
    } else {
      setIsSunk();
      return 0; // return 0 if ship is destroyed.
    }
  };
  const getLength = () => length;

  const getIsSunk = () => isSunk;
  const getIsVertical = () => isVertical;
  const setIsSunk = () => (isSunk = true);

  const getGridPosition = () => gridPosition;
  const setGridPosition = (newGrid) => (gridPosition = newGrid);

  return {
    id,
    hit,
    getLives,
    getLength,
    getGridPosition,
    setGridPosition,
    getIsSunk,
    setIsSunk,
    getIsVertical,
  };
};

export { Ship };
