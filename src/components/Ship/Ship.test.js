import {Ship} from "./Ship";

test("Ship is created correctly", () => {
  expect(Ship(3, [3, 4, 5]).getLength()).toEqual(3);
  expect(Ship(3, [3, 4, 5]).getGridPosition()).toEqual([3, 4, 5]);
});

test("Ship removes a life when hit", () => {
  expect(Ship(3, [3, 4, 5]).hit()).toEqual(2);
});
