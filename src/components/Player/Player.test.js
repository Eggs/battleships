import Player from "./Player";

test("Player contains a way of storing moves made", () => {
  expect(Player().movesMade).toBeDefined();
});

test("Player can make a random play", () => {
  const player = Player();
  player.randomMove();
  expect(player.movesMade.length).toBeGreaterThan(0);
});
