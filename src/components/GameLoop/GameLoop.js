//import Player from "../Player/Player";
import GameBoardContainer from "../GameBoard/GameBoardContainer";
import GameBoard from "../GameBoard/GameBoard";
import Divider from "@material-ui/core/Divider";

const GameLoop = () => {
  // const player1 = Player();
  // const player2 = Player();

  const player1Game = GameBoardContainer();
  const player2Game = GameBoardContainer();

  return (
    <div>
      <h1>Player one board: </h1>
      <GameBoard
        gameBoard={player1Game.gameBoard}
        ships={player1Game.ships}
        setShips={player1Game.setShips}
        addShip={player1Game.addShip}
        receiveAttack={player1Game.receiveAttack}
        checkIfAllShipsAreSunk={player1Game.checkIfAllShipsAreSunk}
      />
      <Divider />
      <h1>Player two board: </h1>
      <GameBoard
        gameBoard={player2Game.gameBoard}
        ships={player2Game.ships}
        setShips={player2Game.setShips}
        addShip={player2Game.addShip}
        receiveAttack={player2Game.receiveAttack}
        checkIfAllShipsAreSunk={player2Game.checkIfAllShipsAreSunk}
      />
    </div>
  );
};

export default GameLoop;
